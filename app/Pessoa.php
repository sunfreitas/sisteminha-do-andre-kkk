<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Entidade de pessoas físicas ou jurídicas
 */
class Pessoa extends Model
{
	const TIPO = [
		1 => 'Pessoa Jurídica',
		2 => 'Pessoa Física',
		3 => 'Técnico'
	];

    protected $table = 'pessoa';

    protected $fillable = [
		// int
		'tipo',
		// int
		'cpf_cnpj',
		// string(250)
		'nome', 
		// string(150)
		'email',
		// string(50)
		'telefone',
		// string(10)
		'cep', 
		// string(50)
		'endereco', 
		// string(150)
		'complemento', 
		// string(150)
		'bairro', 
		// string(150)
		'cidade',
		// string(10)
		'uf', 
		
		/** campos de pessoa fisica */		
		// string(20)
		'inscricao_estadual',
		// string(250)
		'razao_social',
		
		/** campos de pessoa juridica */
		// string(50)
		'rg',
		// string(10)
		'rg_uf', 
		// string(50)
		'rg_orgao'
	];

	public function matricula() {		
        return $this->hasOne('App\Matricula', 'pessoa_id');
	}

	public function tipo_descricao() {
		return self::TIPO[$this->tipo];
	}
}
