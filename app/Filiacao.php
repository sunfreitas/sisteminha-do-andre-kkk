<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Registro de pagamento de filiação.
 */
class Filiacao extends Model
{
    const STATUS = [
        '1' => 'Lançado e Efetivado',
        '2' => 'Não Lançado por Falta de Margem Temporariamente',
        '3' => 'Não Lançado por Outros Motivos(Ex.Matricula não encontrada; Mudança de órgão)',
        '4' => 'Lançado com Valor Diferente',
        '5' => 'Não Lançado por Problemas Técnicos',
        '6' => 'Lançamento com Erros',
        'S' => 'Não Lançado: Compra de Dívida ou Suspensão SEAD'
    ];

    protected $table = 'filiacao';
    
    protected $fillable = [
        // string(1)
        'status',
        // integer
        'mes',
        // integer
        'ano',
        // decimal(18,2)
        'valor'
        ];

    public function matricula()
    {
        return $this->belongsTo('App\Matricula', 'matricula_id');
    }
}
