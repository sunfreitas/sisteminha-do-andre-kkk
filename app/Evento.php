<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Registra um evento no sistema, podendo ser criação, alteração ou exclusão de alguma entidade
 */
class Evento extends Model
{
    protected $table = 'evento';

    protected $fillable = [
        // string(50) - nome da entidade relacionada ao evento
        'entidade',
        // string(50) - chave primaria da entidade
        'pk',
        // datetime
        'data',
        // string(500)
        'descricao',
        // string(50) - usuario que gerou o evento
        'usuario'
    ];

}
