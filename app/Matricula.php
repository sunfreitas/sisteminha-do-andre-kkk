<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Representa uma matrícula de técnico
 */
class Matricula extends Model
{
    const SITUACAO = [
        1 => 'Ativo',
        2 => 'Inativo',
        3 => 'Pensionista'
    ];

    protected $table = 'matricula';

    protected $keyType = 'string';

    protected $fillable = [
        // int
        'situacao',
        // string(250)
        'lotacao',
        // string(150)
        'classe', 
        // string(150)
        'nivel', 
        // date
		'data_admissao',
        
        /** campos de filiado */
        //boolean
        'filiado',
        // date
        'data_filiacao',
        // date
		'data_desfiliacao'
	];

    public function pessoa()
    {
        return $this->belongsTo('App\Pessoa', 'pessoa_id');
    }
}