<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoContaPagar;

class TipoContaPagarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contas = TipoContaPagar::all();
        return view('tipocontaspagar.index', ['contas' => $contas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipocontaspagar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $conta = TipoContaPagar::create( $request->except('_token') );

        return redirect()->route( 'tipocontapagar.show', ['id' => $conta->id ] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $conta = TipoContaPagar::find($id);
        return view('tipocontaspagar.show', ['conta' => $conta]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $conta = TipoContaPagar::find($id);
        return view('tipocontaspagar.edit', ['conta' => $conta]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $conta = TipoContaPagar::find($id);

        $conta->fill( $request->except('_token') );

        $conta->save();

        return redirect()->route('tipocontapagar.show', ['id' => $conta->id] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
