<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filiado;
use App\Pessoa;
use App\Http\Requests\StoreFiliado;

class FiliadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filiados = Filiado::all();

        return view('filiado.index', ['filiados' => $filiados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('filiado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFiliado $request)
    {   
        Request::flash();
        $pessoa = Pessoa::create( $request->input('pessoa') );
        $filiado = $pessoa->filiado()->create($request->input('filiado'));

        return redirect()->route( 'filiado.show', ['id' => $filiado->id ] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $filiado = Filiado::find($id);
        return view('filiado.show', ['filiado' => $filiado]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $filiado = Filiado::find($id);
        return view('filiado.edit', ['filiado' => $filiado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filiado = Filiado::find($id);
        $pessoa = $filiado->pessoa;

        $filiado->fill( $request->input('filiado') );
        $filiado->save();

        $pessoa->fill( $request->input('pessoa') );
        $pessoa->save();


        return redirect()->route('filiado.show', ['id' => $filiado->id] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Muda o valor do campo 'filiado' para true.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ativar($id)
    {
        $filiado = Filiado::find($id);
        $filiado->filiado = true;
        $filiado->save();
        
        return redirect()->route('filiado.index');
    }
}
