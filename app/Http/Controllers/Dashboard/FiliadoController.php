<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Filiado;

class FiliadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filiados = Filiado::all();

        return view('dashboard.filiado.index', ['filiados' => $filiados]);
    }

    public function create()
    {
        return view('dashboard.filiado.create');
    }

}
