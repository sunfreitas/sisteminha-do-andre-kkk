<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContasPagar;
use App\FormaPagamento;

class ContaPagarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contas = ContasPagar::all();
        return view('contaspagar.index', ['contas' => $contas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $formas = FormaPagamento::all();
        return view('contaspagar.create',['formas'=>$formas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $conta = ContasPagar::create( $request->except('_token') );

        return redirect()->route( 'contaspagar.show', ['id' => $conta->id ] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $conta = ContasPagar::find($id);
        /*$phone = User::find(1)->phone;*/
        return view('contaspagar.show', ['conta' => $conta]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $conta = ContasPagar::find($id);
        $formas = FormaPagamento::all();
        return view('contaspagar.edit', ['conta' => $conta, 'formas' => $formas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $conta = ContasPagar::find($id);

        $conta->fill( $request->except('_token') );

        $conta->save();

        return redirect()->route('contaspagar.show', ['id' => $conta->id] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
