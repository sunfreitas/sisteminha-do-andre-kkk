<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use App\Filiacao;

class FiliacaoController extends Controller
{
    public function index(Request $request)
    {
        $status = $request->session()->get('status');
        return view('filiacao.index', ['status' => $status] );
    }

    public function import(Request $request)
    {
        $file = $request->file('arquivo');

        $handle = fopen($file->getRealPath(), "r");
        $mes = '';
        $ano = '';

        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                
                if ( preg_match('/(\w+): [0-9]{2}[\/a?][0-9]{4}/', $line) ) {
                    $mes = substr($line, 60, 2);
                    $ano = substr($line, 63, 5);
                }

                if ( preg_match('/[0-9]{6}-[0-9]{1}/', $line) ) {
                    $pagamento = Pagamento::create(
                        [
                            'matricula_id' => trim(substr($line, 7, 8)),
                            'status' => trim(substr($line, 0, 6)),
                            'valor' => trim(str_replace(',', '.', substr($line, 109, 12))),
                            'mes' => trim($mes),
                            'ano' => trim($ano)
                        ]
                    );
                }
            }

            fclose($handle);
        }
        $request->session()->flash('status', true);
        return redirect()->route('filiacao.index');
    }
}
