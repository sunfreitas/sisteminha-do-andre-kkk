<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormaPagamento;

class FormaPagamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formaPagamentos = FormaPagamento::all();
        return view('formapagamento.index', ['formaPagamentos' => $formaPagamentos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('formapagamento.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formaPagamento = FormaPagamento::create( $request->except('_token') );

        return redirect()->route( 'formapagamento.show', ['id' => $formaPagamento->id ] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formaPagamento = FormaPagamento::find($id);
        return view('formapagamento.show', ['formaPagamento' => $formaPagamento]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $formaPagamento = FormaPagamento::find($id);
        return view('formapagamento.edit', ['formaPagamento' => $formaPagamento]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formaPagamento = FormaPagamento::find($id);

        $formaPagamento->fill( $request->except('_token') );

        $formaPagamento->save();

        return redirect()->route('formapagamento.show', ['id' => $formaPagamento->id] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
