<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFiliado extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "pessoa['cpfCnpj']" => "required",
            "pessoa['nome']" => "required",
            "filiado['matricula']" => "required"
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "pessoa['cpfCnpj'].required" => 'Preencha o campo "CPF"',
            "pessoa['nome'].required" => 'Preencha o campo "Nome Completo"',
            "filiado['matricula'].required" => 'Preencha o campo "Matrícula"'
        ];
    }
}
