<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Forma de pagamento de Conta
 */
class FormaPagamento extends Model
{
    protected $table = 'forma_pagamento';

    protected $fillable = [
        // string(150)
        'descricao', 
        // boolean
        'ativo'
    ];

}
