<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Categoria de Conta.
 */
class TipoConta extends Model
{    
    protected $table = 'tipo_conta';

    protected $fillable = [
        // int - Conta::CATEGORIA
        'categoria',
        // string(150)
        'descricao', 
        // boolean
        'ativo'
    ];
}
