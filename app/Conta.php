<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Entidade de contas a pagar ou receber
 */
class Conta extends Model
{
    const CATEGORIA = [
        1 => 'Conta a Pagar',
        2 => 'Conta a Receber'
    ];

    const STATUS = [
        1 => 'Aberta',
        2 => 'Paga',
        3 => 'Cancelada'
    ];
    
    protected $table = 'conta';
    
    protected $fillable = [
        // int
        'categoria',
        // int
        'status',
        // string(150)
        'descricao',
        // decimal(18,2)
        'valor',
        // date
        'data_vencimento',
        // string(150)
        'observacao',

        /** campos de pagamento */
        // date
        'data_pagamento',
        // decimal(18,2)
        'valor_pago'
    ];

    public function pessoa()
    {
        return $this->hasOne('App\Pessoa', 'pessoa_id');
    }

    public function tipo()
    {
        return $this->hasOne('App\TipoConta', 'tipo_conta_id');
    }

    public function forma_pagamento()
    {
        return $this->hasOne('App\FormaPagamento', 'forma_pagamento_id');
    }
}
