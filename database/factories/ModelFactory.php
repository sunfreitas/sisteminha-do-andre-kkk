<?php


$factory->define(App\Pessoa::class, function (Faker\Generator $faker){
	return [
		'tipo_pessoa' => 0,
		'cpfCnpj' => $faker->biasedNumberBetween($min = 0, $max = 999, $function = 'sqrt'),
		'rg' => $faker->biasedNumberBetween($min = 0, $max = 999, $function = 'sqrt'),
		'orgao_expeditor' => $faker->stateAbbr,
		'nome' => $faker->name,
		'endereco' => $faker->address,
		'bairro' => $faker->streetName,
		'cidade' => $faker->city,
		'uf' => $faker->stateAbbr,
		'cep'=> '64000-000',
		'telefone' => $faker->tollFreePhoneNumber,
		'email' => $faker->email,
		'data_nascimento' => $faker->date($format = 'Y-m-d'),
		'inscricao_estadual' => $faker->biasedNumberBetween($min = 0, $max = 999, $function = 'sqrt'),
		'razao_social' => $faker->name,
	];
});

$factory->define(App\Filiado::class, function (Faker\Generator $faker){
	return [
		'filiado' => true,
		'data_filiacao' => $faker->date($format = 'Y-m-d', $max = 'now'),
		'local_trabalho' => $faker->company,
		'cargo' => $faker->jobTitle,
		'classe' => $faker->catchPhrase,
		'nivel' => $faker->biasedNumberBetween($min = 0, $max = 999, $function = 'sqrt'),
		'matricula' => $faker->biasedNumberBetween($min = 0, $max = 999, $function = 'sqrt'),
		'data_admissao' => $faker->date($format = 'Y-m-d', $max = 'now'),
		'pessoa_id' => function () {
            return factory(App\Pessoa::class)->create()->id;
        }
	];
} );

$factory->define(App\TipoContaReceber::class, function(Faker\Generator $faker){
	return [
		'codigo' => 001,
		'descricao' => 'Filiação',
	];
});

$factory->define(App\TipoContaPagar::class, function(Faker\Generator $faker){
	return [
		'codigo' => $faker->biasedNumberBetween($min = 0, $max = 999, $function = 'sqrt'),
		'descricao' => $faker->sentence($nbWords = 6, $variableNbWords = true),
	];
});

$factory->define(App\FormaPagamento::class, function(Faker\Generator $faker){
	return [
		'codigo' => $faker->biasedNumberBetween($min = 0, $max = 999, $function = 'sqrt'),
		'descricao' => $faker->sentence($nbWords = 6, $variableNbWords = true),
	];
});

$factory->define(App\ContasPagar::class, function(Faker\Generator $faker){
	return [
		'codigo' => $faker->biasedNumberBetween($min = 0, $max = 999, $function = 'sqrt'),
		'descricao' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'parcela' => $faker->biasedNumberBetween($min = 0, $max = 12, $function = 'sqrt'),
        'valor' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100), // 48.89,
        'data_vencimento' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'forma_pagamento' => function(){
        	return App\FormaPagamento::find(1)->id;
        }
	];
});

$factory->define(App\ContasReceber::class, function(Faker\Generator $faker){
	return [
		'codigo' => $faker->biasedNumberBetween($min = 0, $max = 999, $function = 'sqrt'),
		'descricao' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'parcela' => $faker->biasedNumberBetween($min = 0, $max = 12, $function = 'sqrt'),
        'valor' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100), // 48.89,
        'data_vencimento' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'forma_pagamento' => function(){
        	return App\FormaPagamento::find(1)->id;
        }
	];
});