<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pessoa_id')->unsigned();
            $table->smallInteger('tipo_conta_id')->unsigned();
            $table->smallInteger('categoria');
            $table->smallInteger('status');
            $table->string('descricao', 150);
            $table->decimal('valor', 18, 2);
            $table->dateTime('data_vencimento');
            $table->string('observacao', 150);
            
            $table->smallInteger('forma_pagamento_id')->unsigned();
            $table->dateTime('data_pagamento');
            $table->decimal('valor_pago', 18, 2);

            $table->foreign('pessoa_id')->references('id')->on('pessoa');
            $table->foreign('tipo_conta_id')->references('id')->on('tipo_conta');
            $table->foreign('forma_pagamento_id')->references('id')->on('forma_pagamento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conta');
    }
}
