<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPessoa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo')->unsigned();
            $table->integer('cpf_cnpj')->unsigned();
            $table->string('nome', 150);
            $table->string('email', 150)->unique();
            $table->string('telefone', 50);

            $table->string('cep', 10);
            $table->string('endereco', 50);
            $table->string('complemento', 150);
            $table->string('bairro', 150);
            $table->string('cidade', 150);
            $table->string('uf', 10);

            $table->string('inscricao_estadual', 20);
            $table->string('razao_social', 250);

            $table->string('rg', 50);
            $table->string('rg_uf', 10);
            $table->string('rg_orgao', 50);

            $table->unique('cpf_cnpj');

            $table->index('cpf_cnpj');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoa');
    }
}
