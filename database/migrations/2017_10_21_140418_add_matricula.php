<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMatricula extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matricula', function (Blueprint $table) {
            $table->string('id', 10);
            $table->integer('pessoa_id')->unsigned();
            $table->smallInteger('situacao');
            $table->string('lotacao', 250)->nullable();
            $table->string('classe', 250)->nullable();
            $table->string('nivel', 250)->nullable();
            $table->date('data_admissao')->nullable();            
            $table->boolean('filiado')->default(false);
            $table->date('data_filiacao')->nullable();
            $table->date('data_desfiliacao')->nullable();
            
            $table->primary('id');

            $table->foreign('pessoa_id')->references('id')->on('pessoa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matricula');
    }
}
