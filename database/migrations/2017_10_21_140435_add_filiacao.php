<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiliacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filiacao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('matricula_id', 10);
            $table->string('status', 1);
            $table->smallInteger('mes');
            $table->smallInteger('ano');
            $table->decimal('valor', 18, 2);

            $table->foreign('matricula_id')->references('id')->on('matricula');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filiacao');
    }
}
