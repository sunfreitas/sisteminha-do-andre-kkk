<?php

use Illuminate\Database\Seeder;
use App\FormaPagamento;

class FormaPagamentoTableSeeder extends Seeder
{
    public function run()
    {
        FormaPagamento::truncate();

        FormaPagamento::insert([
            'descricao' => 'Dinheiro',
            'ativo' => true
        ]);
        FormaPagamento::insert([
            'descricao' => 'Cheque',
            'ativo' => true
        ]);
        FormaPagamento::insert([
            'descricao' => 'Transferência',
            'ativo' => true
        ]);
        FormaPagamento::insert([
            'descricao' => 'DOC/TED',
            'ativo' => true
        ]);
    }
}
