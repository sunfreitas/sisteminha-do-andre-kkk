<?php

use Illuminate\Database\Seeder;
use App\TipoConta;

class TipoContaTableSeeder extends Seeder
{
    public function run()
    {
        TipoConta::truncate();

        TipoConta::insert([
            'categoria' => 1,
            'descricao' => 'Recebimento 1',
            'ativo' => true
        ]);
        TipoConta::insert([
            'categoria' => 1,
            'descricao' => 'Recebimento 2',
            'ativo' => true
        ]);
        TipoConta::insert([
            'categoria' => 2,
            'descricao' => 'Pagamento 1',
            'ativo' => true
        ]);
        TipoConta::insert([
            'categoria' => 2,
            'descricao' => 'Pagamento 2',
            'ativo' => true
        ]);
    }
}
