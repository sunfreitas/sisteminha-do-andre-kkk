<?php

use Illuminate\Database\Seeder;
use App\Evento;
use App\Conta;
use App\Filiacao;
use App\Matricula;
use App\Pessoa;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET foreign_key_checks=0");

        // seed de usuarios
        // $this->call(UsersTableSeeder::class);
        
        // seeds opcionais
        $this->call(FormaPagamentoTableSeeder::class);
        $this->call(TipoContaTableSeeder::class);
        
        // seeds de limpeza
        Evento::truncate();
        Conta::truncate();
        Filiacao::truncate();
        Matricula::truncate();
        Pessoa::truncate();

        DB::statement("SET foreign_key_checks=1");
    }
}
