<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// FormaPagamento
Route::get('formapagamento/{id}/enable', 'FormaPagamentoController@ativar')->name('formapagamento.enable');
Route::get('formapagamento/{id}/disable', 'FormaPagamentoController@desativar')->name('formapagamento.disable');
Route::resource('formapagamento', 'FormaPagamentoController', ['except' => [
    'destroy'
]]);

// TipoConta
Route::get('tipoconta/{id}/enable', 'TipoContaController@ativar')->name('tipoconta.enable');
Route::get('tipoconta/{id}/disable', 'TipoContaController@desativar')->name('tipoconta.disable');
Route::resource('tipoconta', 'TipoContaController', ['except' => [
    'destroy'
]]);

// Conta
Route::resource('conta', 'ContaController', ['except' => [
    'destroy'
]]);

// Pessoa
Route::view('pessoa/juridica', 'pessoa.juridica')->name('pessoa.juridica');
Route::view('pessoa/fisica', 'pessoa.fisica')->name('pessoa.fisica');
Route::view('pessoa/tecnico', 'pessoa.tecnico')->name('pessoa.tecnico');
Route::get('pessoa/{id}/upgrade', 'PessoaController@upgrade')->name('pessoa.upgrade');
Route::resource('pessoa', 'PessoaController', ['except' => [
    'destroy'
]]);

// Filiacao
Route::get('pagamentos', 'FiliacaoController@index')->name('filiacao.index');
Route::post('pagamentos/import', 'FiliacaoController@import')->name('filiacao.import');
