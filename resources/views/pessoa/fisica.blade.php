@extends('layouts.dashboard')

@section('title', 'Adicionar Pessoa Física')

@section('content')
	<form action="{{ route('pessoa.store') }}" method="POST">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div>
						<h3 class="card-title m-b-5"><span class="lstick"></span>Adicionar Pessoa Física</h3>
					</div>
					{{ csrf_field() }}
					<div class="panel panel-default">
						<div class="panel-body">
							<input name="pessoa[tipo]" type="hidden" value="2" />

							<div class="row">
								<div class="form-group col-md-3">
									<label>CPF *</label>
									<input type="text" name="pessoa[cpf_cnpj]" class="form-control" required />
								</div>
								
								<div class="form-group col-md-3">
									<label>RG</label>
									<input type="text" name="pessoa[rg]" class="form-control" />
								</div>
								
								<div class="form-group col-md-3">
									<label>UF</label>
									<input type="text" name="pessoa[rg_uf]" class="form-control" />
								</div>

								<div class="form-group col-md-3">
									<label>Órgão Expedidor</label>
									<input type="text" name="pessoa[rg_orgao]" class="form-control" />
								</div>
							</div>
							
							@include('shared.pessoa')
						</div>
					</div>
				</div>
				<input type="submit" class="btn btn-success" value="Cadastrar">
			</div>
		</div>
	</form>			
@endsection

@section('scripts')
@endsection