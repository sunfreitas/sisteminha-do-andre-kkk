@extends('layouts.dashboard')

@section('title', 'Adicionar Pessoa Jurídica')

@section('content')
	<form action="{{ route('pessoa.store') }}" method="POST">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div>
						<h3 class="card-title m-b-5"><span class="lstick"></span>Adicionar Pessoa Jurídica</h3>
					</div>
					{{ csrf_field() }}
					<div class="panel panel-default">
						<div class="panel-body">
							<input name="pessoa[tipo]" type="hidden" value="1" />

							<div class="row">
								<div class="form-group col-md-3">
									<label>CNPJ *</label>
									<input type="text" name="pessoa[cpf_cnpj]" class="form-control" required />
								</div>
								
								<div class="form-group col-md-3">
									<label>Inscrição Estadual</label>
									<input type="text" name="pessoa[inscricao_estadual]" class="form-control" />
								</div>
								
								<div class="form-group col-md-6">
									<label>Razão Social</label>
									<input type="text" name="pessoa[razao_social]" class="form-control" />
								</div>
							</div>
							
							@include('shared.pessoa')
						</div>
					</div>
				</div>

				<input type="submit" class="btn btn-success" value="Cadastrar">
			</div>
		</div>
	</form>			
@endsection

@section('scripts')
@endsection