@extends('layouts.dashboard')

@section('title', 'Adicionar Técnico')

@section('content')
	<form action="{{ route('pessoa.store') }}" method="POST">
		<div class="col-md-12">

			<div class="card">
				<div class="card-body">
					<div>
						<h3 class="card-title m-b-5"><span class="lstick"></span>Adicionar Técnico</h3>
					</div>
					{{ csrf_field() }}
					<div class="panel panel-default">
						<div class="panel-body">
							<input name="pessoa[tipo]" type="hidden" value="3" />

							<div class="row">
								<div class="form-group col-md-3">
									<label>CPF *</label>
									<input type="text" name="pessoa[cpf_cnpj]" class="form-control" required />
								</div>
								
								<div class="form-group col-md-3">
									<label>RG</label>
									<input type="text" name="pessoa[rg]" class="form-control" />
								</div>
								
								<div class="form-group col-md-3">
									<label>UF</label>
									<input type="text" name="pessoa[rg_uf]" class="form-control" />
								</div>

								<div class="form-group col-md-3">
									<label>Órgão Expedidor</label>
									<input type="text" name="pessoa[rg_orgao]" class="form-control" />
								</div>
							</div>
							
							@include('shared.pessoa')
						</div>
					</div>
				</div>
			</div>
			
			<div class="card">
				<div class="card-body">
					<div>
						<h3 class="card-title m-b-5"><span class="lstick"></span>Dados Funcionais</h3>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">
								<div class="form-group col-md-2">
									<label>Matrícula *</label>
									<input type="text" name="matricula[id]" class="form-control" placeholder="000000-0" value="{{ old('matricula.id') }}">
								</div>
								<div class="form-group col-md-4">
									<label>Situação</label>
									<input type="text" name="matricula[situacao]" class="form-control" placeholder="" value="{{ old('matricula.situacao') }}">
								</div>
								<div class="form-group col-md-4">
									<label>Lotação</label>
									<input type="text" name="matricula[lotacao]" class="form-control" placeholder="" value="{{ old('matricula.lotacao') }}">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Classe</label>
									<input type="text" name="matricula[classe]" class="form-control" placeholder="" value="{{ old('matricula.classe') }}">
								</div>
								<div class="form-group col-md-4">
									<label>Nível</label>
									<input type="text" name="matricula[nivel]" class="form-control" placeholder="" value="{{ old('matricula.nivel') }}">
								</div>
								<div class="form-group col-md-4">
									<label>Data de admissão</label>
									<input type="date" name="matricula[data_admissao]" class="form-control data" placeholder="" value="{{ old('matricula.data_admissao') }}">
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="card">
				<div class="card-body">
					<div>
						<h3 class="card-title m-b-5"><span class="lstick"></span>Filiação</h3>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">
								<div class="form-group col-md-3">
									<input type="checkbox" name="matricula[filiado]" class="form-control" value="{{ old('matricula.id') }}" />
									<label>Filiado</label>
								</div>
								<div class="form-group col-md-4">
									<label>Data de Filiação</label>
									<input type="date" name="matricula[data_filiacao]" class="form-control" placeholder="" value="{{ old('matricula.data_filiacao') }}" />
								</div>
								<div class="form-group col-md-4">
									<label>Data de Desfiliação</label>
									<input type="date" name="matricula[data_desfiliacao]" class="form-control" placeholder="" value="{{ old('matricula.data_desfiliacao') }}" />
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>

			<input type="submit" class="btn btn-success" value="Cadastrar">
		</div>
	</form>
@endsection

@section('scripts')
@endsection