
<div class="row">
	<div class="form-group col-md-6">
		<label>Nome *</label>
		<input type="text" name="pessoa[nome]" class="form-control" placeholder="" required>
	</div>

	<div class="form-group col-md-3">
		<label>Telefone</label>
		<input type="text" name="pessoa[telefone]" class="form-control fone" placeholder="(86) 0000-0000">
	</div>

	<div class="form-group col-md-3">
		<label>e-mail</label>
		<input type="email" name="pessoa[email]" class="form-control email" placeholder="">
	</div>
</div>

<div class="row">
	<div class="form-group col-md-2">
		<label>CEP</label>
		<input type="text" name="pessoa[cep]" class="form-control cep" placeholder="64000-000" data-mask="99999-999">
	</div>

	<div class="form-group col-md-5">
		<label>Endereço</label>
		<input type="text" name="pessoa[endereco]" class="form-control" placeholder="">
	</div>

	<div class="form-group col-md-5">
		<label>Complemento</label>
		<input type="text" name="pessoa[complemento]" class="form-control" placeholder="">
	</div>

</div>

<div class="row">	
	<div class="form-group col-md-5">
		<label>Bairro</label>
		<input type="text" name="pessoa[bairro]" class="form-control" placeholder="">
	</div>	

	<div class="form-group col-md-5">
		<label>Cidade</label>
		<input type="text" name="pessoa[cidade]" class="form-control" placeholder="">
	</div>

	<div class="form-group col-md-2">
		<label>UF</label>
		<input type="text" name="pessoa[uf]" class="form-control" placeholder="">
	</div>
</div>