@extends('layouts.dashboard')

@section('title', 'Dados da Forma de Pagamento')

@section('header-left-controls')
	<!-- Controles da esquerda -->
@endsection

@section('page-header', 'Dados da Forma de Pagamento')

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">

						<h3 class="card-title m-b-5 pull-left"><span class="lstick"></span>Forma de Pagamento</h3>

						<div class="pull-right">
							<a href="{{ route('formapagamento.edit', ['id' => $formaPagamento->id]) }}" class="btn btn-primary"><span class="mdi mdi-account-edit"></span> Editar</a>
							<a href="#" class="btn btn-danger"><span class="mdi mdi-delete"></span>Excluir</a>	
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<p>ID: {{ $formaPagamento->id }}</p>
						<p>Código: {{ $formaPagamento->codigo }}</p>
						<p>Descrição: {{ $formaPagamento->descricao }}</p>
						<p>Ativo: @if($formaPagamento->ativo) Sim @else Não @endif</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
@endsection