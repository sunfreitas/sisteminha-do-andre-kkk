@extends('layouts.dashboard')

@section('title', 'Formas de Pagamento')

@section('header-left-controls')
	<!-- Controles da esquerda -->
@endsection

@section('page-header', 'Formas de Pagamento')

@section('header-right-controls')
	<a href="{{ route('formapagamento.create') }}" class="btn btn-primary">Novo</a>
@endsection

@section('content')
<table id="formaspagamento" class="table table-striped display">
	<thead>
		<tr>
			<th> <input type="checkbox" name="select-all-customer"> </th>
			<th>ID</th>
			<th>Código</th>
			<th>Descrição</th>
		</tr>
	</thead>
	<tbody>

		@foreach ($formaPagamentos as $formaPagamento)
			<tr>
				<td> <input type="checkbox" name="select-customer"> </td>
				<td><a href="{{ route('formapagamento.show', ['id' => $formaPagamento->id]) }}">{{ $formaPagamento->id }}</a></td>
				<td>{{ $formaPagamento->codigo }}</td>
				<td>{{ $formaPagamento->descricao }}</td>
			</tr>
		@endforeach

	</tbody>
</table>

@endsection

@section('scripts')

	<script src="{{ asset('js/datatables.min.js') }}"></script>
	<script type="text/javascript">
	  $(document).ready(function() {
	    $('#formaspagamento').DataTable({
	    	'columnDefs': [
    			{"searchable": true, "targets": 2}
	    	],
	    	'language':{
	    		"search":"Buscar registros",
	    		"emptyTable": "Não registros",
	    		"zeroRecords": "Não foram encontrados registros",
	    		"lengthMenu": "Exbir _MENU_ registros",
	    		"infoFiltered": "(filtrados de _MAX_ registros)",
	    		"info": "Exibindo _START_ a _END_ de _TOTAL_ registros",
	    		"infoEmpty": "Exibindo 0 a 0 de 0 registros",
	    		"paginate": {
	    			"first": "Primeira página",
	    			"last": "Última página",
	    			"next": "Próximo",
	    			"previous": "Anterior"
	    		}
	    	}
	    });
	  } );
	</script>

@endsection