@extends('layouts.dashboard')

@section('title', 'Editar Forma de Pagamento')

@section('header-left-controls')
	
@endsection

@section('page-header', 'Editar Forma de Pagamento')

@section('content')
	<div class="col-md-12">
		<form action="{{ route('formapagamento.update', ['id' => $formaPagamento->id ]) }}" id="editar_conta_receber" name="cadastro_conta" method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
	    	
	    	<div class="panel panel-default">
			  <div class="panel-heading">Dados da conta</div>
			  	<div class="panel-body">

					<div class="form-group">
			   			<label>Código</label>
						<input type="text" name="codigo" class="form-control" placeholder="Código" value="{{ $formaPagamento->codigo }}" required="required">
					</div>

					<div class="form-group">
			   			<label>Descrição</label>
						<input type="text" name="descricao" class="form-control" placeholder="Descrição do tipo de conta" value="{{ $formaPagamento->descricao }}">
					</div>
					
			    	<div class="row">
						<div class="form-group">
							<div class="demo-checkbox">
								<input type="checkbox" id="basic_checkbox_2" value="1" class="filled-in" @if( $formaPagamento->ativo ) checked="checked" @endif>
								<label for="basic_checkbox_2">Ativo</label>
							</div>
							<input type="hidden" id="ativo" name="ativo" value="{{ $formaPagamento->ativo }}">
						</div>	
					</div>

			  	</div>
			</div>

			<input type="submit" class="btn btn-success" value="Atualizar">
		</form>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(function(){
			$('#basic_checkbox_2').change(function(){
				if ( $(this).prop('checked') ) {
					$('#ativo').val(1);	
				}else{
					$('#ativo').val(0);
				}
			});

		});
	</script>
@endsection