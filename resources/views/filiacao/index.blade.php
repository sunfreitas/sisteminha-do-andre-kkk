@extends('layouts.dashboard')

@section('title', 'Pagamento de Filiados')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('header-left-controls')
	
@endsection

@section('page-header', 'Pagamento de Filiados')

@section('content')
	<div class="col-md-12">
		@if( $status )
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Operação realizada com sucesso!</strong>
		</div>
		@endif
		<div class="card">
			<div class="card-body">
				
				<h3 class="card-title m-b-5"><span class="lstick"></span>Importar Pagamentos</h3>
                <h6 class="card-subtitle">Upload do arquivo com informações de pagamentos de filiados</h6>
				
				<form action="{{ route('filiacao.import') }}" method="POST"  enctype="multipart/form-data" name="form_pagamento" id="form_pagamento">
			
					{{ csrf_field() }}

			    	<div class="panel panel-default">
					  	<div class="panel-body">

					   		<div class="form-group">
                				<input type="file" id="input-file-now" class="dropify" name="arquivo"/>
					   		</div>

					  	</div>
					</div>

					<input type="submit" class="btn btn-success" value="Enviar arquivo">
				</form>
                

			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			$('.dropify').dropify({
            messages: {
                default: 'Arraste um arquivo ou clique aqui para realizar o upload',
                replace: 'Arraste outro arquivo ou clique aqui para substituir',
                remove: 'Remover arquivo',
                error: 'Desculpe, mas o arquivo é muito grande'
            }
        });
		});
	</script>
@endsection