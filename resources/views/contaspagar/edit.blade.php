@extends('layouts.dashboard')

@section('title', 'Editar conta a pagar')

@section('header-left-controls')
	
@endsection

@section('page-header', 'Editar conta a pagar')

@section('content')
	<div class="col-md-12">
		<form action="{{ route('contaspagar.update', ['id' => $conta->codigo ]) }}" id="editar_conta_pagar" name="editar_conta_pagar" method="POST">

			<div class="card">
				<div class="card-body">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
	    	
			    	<div class="panel panel-default">
					  <div class="panel-heading">Dados da conta</div>
					  	<div class="panel-body">

					   		<div class="form-group">
					   			<label>Parcela</label>
					   			<input type="text" name="parcela" class="form-control" placeholder="Número da parcela" value="{{ $conta->parcela }}">
					   		</div>

					   		<div class="form-group">
					   			<label>Forma de pagamento</label>
					   			<select name="forma_pagamento" class="form-control">
					   				<option>Forma de pagamento</option>
					   				@foreach ($formas as $forma)
					   				<option value="{{ $forma->id }}" @if($conta->forma_pagamento == $forma->id) selected="selected" @endif >{{ $forma->descricao }}</option>
					   				@endforeach
					   			</select>
					   		</div>

					   		<div class="form-group">
					   			<label>Descrição</label>
					   			<input type="text" name="descricao" class="form-control cpf" placeholder="Descrição da conta" value="{{ $conta->descricao }}">
					   		</div>

					   		<div class="form-group">
					   			<label>Valor (R$)</label>
					   			<input type="text" name="valor" class="form-control" placeholder="Valor (R$)" value="{{ $conta->valor }}">
					   		</div>

					   		<div class="form-group">
					   			<label>Data do Vencimento</label>
					   			<input type="text" name="data_vencimento" class="form-control" placeholder="dd/mm/YYYY" value="{{ $conta->data_vencimento }}">
					   		</div>

					  	</div>
					</div>
				</div>
			</div>
			<input type="submit" class="btn btn-success" value="Atualizar">
		</form>
	</div>
@endsection