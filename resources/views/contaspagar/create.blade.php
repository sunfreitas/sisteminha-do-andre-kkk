@extends('layouts.dashboard')

@section('title', 'Cadastrar ')

@section('header-left-controls')
	
@endsection

@section('page-header', 'Cadastrar Conta')

@section('content')
	<div class="col-md-12">
		<form action="{{ route('contaspagar.store') }}" method="POST" name="cadastro_conta_pagar" id="cadastro_conta_pagar">
			
			{{ csrf_field() }}
			<div class="card">
				<div class="card-body">
					
	    	<div class="panel panel-default">
			  <div class="panel-heading">Dados da conta</div>
			  	<div class="panel-body">

			   		<div class="form-group">
			   			<label>Parcela</label>
			   			<input type="text" name="parcela" class="form-control" placeholder="Número da parcela" required="required">
			   		</div>

			   		<div class="form-group">
			   			<label>Forma de Pagamento</label>
			   			<select name="forma_pagamento" class="form-control" required="required">
			   				<option selected="selected">Forma de pagamento</option>
			   				@foreach ($formas as $forma)
			   				<option value="{{ $forma->id }}">{{ $forma->descricao }}</option>
			   				@endforeach
			   			</select>
			   		</div>

			   		<div class="form-group">
			   			<label>Descrição</label>
			   			<input type="text" name="descricao" class="form-control cpf" placeholder="Descrição da conta" required="required">
			   		</div>

			   		<div class="form-group">
			   			<label>Valor (R$)</label>
			   			<input type="text" name="valor" class="form-control" placeholder="Valor (R$)">
			   		</div>

			   		<div class="form-group">
			   			<label>Data do Vencimento</label>
			   			<input type="text" name="data_vencimento" class="form-control data" placeholder="dd/mm/YYYY" required="required">
			   		</div>

			  	</div>
			</div>
			</div>
			</div>
			<input type="submit" class="btn btn-success" value="Cadastrar">
		</form>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript" src=" {{ asset('js/jquery.mask.min.js') }} "></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.data').mask('00/00/0000');

			$('#cadastro_conta_pagar').on('submit', function(evt){
				evt.preventDefault();

				let data_vencimento = '';

				if (this.data_vencimento.value !== '') {
					data_vencimento = this.data_vencimento.value.substring(6, 10)+"-"+this.data_vencimento.value.substring(3, 5)  + "-" +  this.data_vencimento.value.substring(0, 2);
					this.data_vencimento.value  = data_vencimento;
					console.info(data_vencimento);
				}

				this.submit();
			});
		});
	</script>
@endsection