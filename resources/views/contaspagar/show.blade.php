@extends('layouts.dashboard')

@section('title', 'Dados do conta')

@section('header-left-controls')
	<!-- Controles da esquerda -->
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<h3 class="card-title m-b-5 pull-left"><span class="lstick"></span>Dados da Conta</h3>

						<div class="pull-right">
							<a href="{{ route('contaspagar.edit', ['id' => $conta->codigo]) }}" class="btn btn-primary"><span class="mdi mdi-account-edit"></span> Editar</a>
							<a href="#" class="btn btn-danger"><span class="mdi mdi-delete"></span>Excluir</a>	
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Dados da conta</div>
					<div class="panel-body">
						<p>Código: {{ $conta->codigo }}</p>
						<p>Parcela: {{ $conta->parcela }}</p>
						<p>Descrição: {{ $conta->descricao }}</p>
						<p>Data do vencimento: {{ $conta->data_vencimento }}</p>
						<p>Data do pagamento: {{ $conta->data_pagamento }}</p>
						<p>Desconto: {{ $conta->desconto }}</p>
						<p>Valor pago: R${{ $conta->valor_pago }}</p>
						<p>Forma de pagamento: {{ $conta->forma_pagamento }}</p>
						<p>Observação: {{ $conta->observacao }}</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	
@endsection