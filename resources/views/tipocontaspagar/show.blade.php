@extends('layouts.dashboard')

@section('title', 'Dados do Tipo de Conta a Pagar')

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<h3 class="card-title m-b-5 pull-left"><span class="lstick"></span>Dados do Tipo de Conta a Pagar</h3>

						<div class="pull-right">
							<a href="{{ route('tipocontapagar.edit', ['id' => $conta->id]) }}" class="btn btn-primary"><span class="mdi mdi-account-edit"></span> Editar</a>
							<a href="#" class="btn btn-danger"><span class="mdi mdi-delete"></span>Excluir</a>	
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-body">
						<p><strong class="font-weight-bold">ID:</strong> {{ $conta->id }}</p>
						<p><strong class="font-weight-bold">Código:</strong> {{ $conta->codigo }}</p>
						<p><strong class="font-weight-bold">Descrição:</strong> {{ $conta->descricao }}</p>
						<p><strong class="font-weight-bold">Ativo:</strong> @if($conta->ativo) Sim @else Não @endif</p>
					</div>
				</div>		
			</div>
		</div>
		
	</div>

	
@endsection