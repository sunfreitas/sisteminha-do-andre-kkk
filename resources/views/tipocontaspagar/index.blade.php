@extends('layouts.dashboard')

@section('title', 'Tipos de Contas a Pagar')

@section('header-left-controls')
	<!-- Controles da esquerda -->
@endsection

@section('page-header', 'Tipos de Contas a Pagar')

@section('header-right-controls')
	
@endsection

@section('content')
<div class="card">
	<div class="card-body">
		
		<div class="row">
			<div class="col-md-6">
				<h3 class="card-title m-b-5"><span class="lstick"></span>Tipos de Contas a Pagar</h3>
			</div>
			<div class="col-md-6">
				<a href="{{ route('tipocontapagar.create') }}" class="btn btn-success pull-right"><span class="mdi mdi-account-plus"></span> Novo</a>
			</div>
		</div>
		<div class="table-responsive m-t-40">
			<table id="tipocontaspagar" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0">
				<thead>
					<tr>
						<th>Código</th>
						<th>Descrição</th>
					</tr>
				</thead>
				<tbody>

					@foreach ($contas as $contas)
						<tr>
							<td><a href="{{ route('tipocontapagar.show', ['id' => $contas->id]) }}">{{ $contas->codigo }}</a></td>
							<td>{{ $contas->descricao }}</td>
						</tr>
					@endforeach

				</tbody>
			</table>
		</div>
	</div>
	
</div>

@endsection

@section('scripts')

	<script src="{{ asset('js/datatables.min.js') }}"></script>
	<script type="text/javascript">
	  $(document).ready(function() {
	    $('#tipocontaspagar').DataTable({
	    	'language':{
	    		"search":"Buscar registros",
	    		"emptyTable": "Não registros",
	    		"zeroRecords": "Não foram encontrados registros",
	    		"lengthMenu": "Exbir _MENU_ registros",
	    		"infoFiltered": "(filtrados de _MAX_ registros)",
	    		"info": "Exibindo _START_ a _END_ de _TOTAL_ registros",
	    		"infoEmpty": "Exibindo 0 a 0 de 0 registros",
	    		"paginate": {
	    			"first": "Primeira página",
	    			"last": "Última página",
	    			"next": "Próximo",
	    			"previous": "Anterior"
	    		}
	    	}
	    });
	  } );
	</script>

@endsection