@extends('layouts.dashboard')

@section('title', 'Cadastrar Tipo de Conta a Pagar')

@section('header-left-controls')
	
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h3 class="card-title m-b-5"><span class="lstick"></span>Cadastrar Tipo de Conta a Pagar</h3>
				
				<form action="{{ route('tipocontapagar.store') }}" method="POST" name="cadastro_conta_pagar" id="cadastro_conta_pagar">
					
					{{ csrf_field() }}

			    	<div class="panel panel-default">
					  <div class="panel-heading">Dados Pessoais</div>
					  	<div class="panel-body">
						  		
					   		<div class="form-group">
					   			<label>Código</label>
					   			<input type="text" name="codigo" class="form-control" placeholder="Código da conta. Ex: 002" required="required">
					   		</div>

					   		<div class="form-group">
					   			<label>Descrição</label>
					   			<input type="text" name="descricao" class="form-control cpf" placeholder="Descrição da conta">
					   		</div>

					  	</div>
					</div>

					

					<input type="submit" class="btn btn-success" value="Cadastrar">
				</form>	
			</div>
		</div>
	</div>
@endsection