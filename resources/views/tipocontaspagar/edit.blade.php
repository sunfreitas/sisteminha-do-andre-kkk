@extends('layouts.dashboard')

@section('title', 'Editar Tipo de Conta a Pagar')

@section('header-left-controls')
	
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h3 class="card-title m-b-5"><span class="lstick"></span>Editar Tipo de Conta a Pagar</h3>
				<form action="{{ route('tipocontapagar.update', ['id' => $conta->id ]) }}" id="editar_conta_pagar" name="editar_conta_pagar" method="POST">
					{{ method_field('PUT') }}
					{{ csrf_field() }}
			    	
			    	<div class="panel panel-default">
					  <div class="panel-heading">Dados da Conta</div>
					  	<div class="panel-body">

							<div class="form-group">
					   			<label>Código</label>
								<input type="text" name="codigo" class="form-control" placeholder="Código" value="{{ $conta->codigo }}">
							</div>

							<div class="form-group">
					   			<label>Descrição</label>
								<input type="text" name="descricao" class="form-control" placeholder="Descrição do tipo de conta" value="{{ $conta->descricao }}">
							</div>

					    	<div class="form-group">
					    		<div class="demo-checkbox">
		                            <input type="checkbox" id="basic_checkbox_2" value="1" class="filled-in" @if( $conta->ativo ) checked="checked" @endif>
		                            <label for="basic_checkbox_2">Ativo</label>
		                        </div>
					    		<input type="hidden" id="ativo" name="ativo" value="{{ $conta->ativo }}">
					    	</div>
					  	</div>
					</div>
			</div>
		</div>
			<input type="submit" class="btn btn-success" value="Atualizar">
		</form>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(function(){
			$('#basic_checkbox_2').change(function(){
				if ( $(this).prop('checked') ) {
					$('#ativo').val(1);	
				}else{
					$('#ativo').val(0);
				}
			});
		});
	</script>
@endsection