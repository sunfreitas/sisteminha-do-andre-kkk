@extends('layouts.dashboard')

@section('title', 'Dados do Tipoo de Conta a Receber')

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<h3 class="card-title m-b-5 pull-left"><span class="lstick"></span>Dados do Tipoo de Conta a Receber</h3>

						<div class="pull-right">
							<a href="{{ route('tipocontareceber.edit', ['id' => $conta->codigo]) }}" class="btn btn-primary"><span class="mdi mdi-account-edit"></span> Editar</a>
							<a href="#" class="btn btn-danger"><span class="mdi mdi-delete"></span>Excluir</a>	
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Dados do tipo de conta</div>
					<div class="panel-body">
						
						<p>ID: {{ $conta->id }}</p>
						<p>Código: {{ $conta->codigo }}</p>
						<p>Descrição: {{ $conta->descricao }}</p>
						<p>Ativo: @if($conta->ativo) Sim @else Não @endif</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	
@endsection