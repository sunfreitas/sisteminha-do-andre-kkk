@extends('layouts.dashboard')

@section('title', 'Tipos do Tipo de Conta a Receber')


@section('content')
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<h3 class="card-title m-b-5"><span class="lstick"></span>Tipos do Tipo de Conta a Receber</h3>
			</div>
			<div class="col-md-6">
				<a href="{{ route('tipocontareceber.create') }}" class="btn btn-success pull-right"><span class="mdi mdi-account-plus"></span> Novo</a>
			</div>
		</div>

		<div class="table-responsive m-t-40">
			<table id="tipocontasreceber" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0">
				<thead>
					<tr>
						<th>Código</th>
						<th>Descrição</th>
					</tr>
				</thead>

				<tbody>
				@foreach ($contas as $conta)
					<tr>
						<td><a href="{{ route('tipocontareceber.show', ['id' => $conta->codigo]) }}">{{ $conta->codigo }}</a></td>
						<td>{{ $conta->descricao }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('scripts')

	<script src="{{ asset('js/datatables.min.js') }}"></script>
	<script type="text/javascript">
	  $(document).ready(function() {
	    $('#tipocontasreceber').DataTable({
	    	'language':{
	    		"search":"Buscar registros",
	    		"emptyTable": "Não registros",
	    		"zeroRecords": "Não foram encontrados registros",
	    		"lengthMenu": "Exbir _MENU_ registros",
	    		"infoFiltered": "(filtrados de _MAX_ registros)",
	    		"info": "Exibindo _START_ a _END_ de _TOTAL_ registros",
	    		"infoEmpty": "Exibindo 0 a 0 de 0 registros",
	    		"paginate": {
	    			"first": "Primeira página",
	    			"last": "Última página",
	    			"next": "Próximo",
	    			"previous": "Anterior"
	    		}
	    	}
	    });
	  } );
	</script>

@endsection