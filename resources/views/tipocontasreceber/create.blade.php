@extends('layouts.dashboard')

@section('title', 'Cadastrar Tipo de Conta a Receber')

@section('header-left-controls')
	
@endsection

@section('page-header', 'Cadastrar Tipo de Conta a Receber')

@section('content')
	<div class="col-md-12">
		<form action="{{ route('tipocontareceber.store') }}" method="POST" name="cadastro_conta_receber" id="cadastro_conta_receber">
			
			{{ csrf_field() }}
			<div class="card">
				<div class="card-body">
					
	    	<div class="panel panel-default">
			  <div class="panel-heading">Dados Pessoais</div>
			  	<div class="panel-body">
				  		
			   		<div class="form-group">
			   			<label>Código</label>
			   			<input type="text" name="codigo" class="form-control" placeholder="Código da conta. Ex: 002" required="required">
			   		</div>

			   		<div class="form-group">
			   			<label>Descrição</label>
			   			<input type="text" name="descricao" class="form-control cpf" placeholder="Descrição da conta">
			   		</div>

			  	</div>
			</div>

			
				</div>
			</div>

			<input type="submit" class="btn btn-success" value="Cadastrar">
		</form>
	</div>
@endsection