@extends('layouts.dashboard')

@section('title', 'Novo Filiado')


@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div>
                    <h3 class="card-title m-b-5"><span class="lstick"></span>Cadastrar Filiado</h3>
                    <h6 class="card-subtitle">Dados Pessoais</h6>

                    @if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

                </div>
				<form action="{{ route('filiado.store') }}" method="POST" name="cadastro_filiado" id="cadastro_filiado">

					{{ csrf_field() }}

					<div class="panel panel-default">
						<div class="panel-heading"></div>
						<div class="panel-body">
                            <input name="pessoa[tipo_pessoa]" type="hidden" value="0">

							<div class="row">
								<div class="form-group col-md-4">
									<label>CPF *</label>
									<input type="text" name="pessoa[cpfCnpj]" class="form-control cpf" value='{{ old( "pessoa.cpfCnpj" ) }}'>
								</div>

								<div class="form-group col-md-8">
									<label>Nome completo *</label>
									<input type="text" name="pessoa[nome]" class="form-control" value='{{ old( "pessoa.nome" ) }}'>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>RG</label>
									<input type="text" name="pessoa[rg]" class="form-control rg" placeholder="" value='{{ old( "pessoa.rg" ) }}'>
								</div>

								<div class="form-group col-md-4">
									<label>Órgão expeditor</label>
									<input type="text" name="pessoa[orgao_expeditor]" class="form-control" placeholder="Exemplo: SSP-PI" value='{{ old( "pessoa.orgao_expeditor" ) }}'>
								</div>

								<div class="form-group col-md-4">
									<label>Data de nascimento</label>
									<input type="text" name="pessoa[data_nascimento]"  id="data_nascimento" class="form-control data" placeholder="dd/mm/aaaa" value='{{ old( "pessoa.data_nascimento" ) }}'>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>CEP</label>
									<input type="text" name="pessoa[cep]" class="form-control cep" placeholder="Exemplo: 64000-000" value='{{ old( "pessoa.cep" ) }}'>
								</div>

								<div class="form-group col-md-8">
									<label>Endereço</label>
									<input type="text" name="pessoa[endereco]" class="form-control" placeholder="Exemplo: Av. Santos Drumond, 200" value='{{ old( "pessoa.endereco" ) }}'>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Bairro</label>
									<input type="text" name="pessoa[bairro]" class="form-control" placeholder="" value='{{ old( "pessoa.bairro" ) }}'>
								</div>
									
								<div class="form-group col-md-6">
									<label>Cidade</label>
									<input type="text" name="pessoa[cidade]" class="form-control" placeholder="" value='{{ old( "pessoa.cidade" ) }}'>
								</div>

								<div class="form-group col-md-2">
									<label>UF</label>
									<input type="text" name="pessoa[uf]" class="form-control" placeholder="" value='{{ old( "pessoa.uf" ) }}'>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Telefone</label>
									<input type="text" name="pessoa[telefone]" class="form-control fone" placeholder="(86) 0000-0000" value='{{ old( "pessoa.telefone" ) }}'>
								</div>

								<div class="form-group col-md-4">
									<label>e-mail</label>
									<input type="text" name="pessoa[email]" class="form-control email" placeholder="" value='{{ old( "pessoa.email" ) }}'>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-body">
					<div>
	                    <h3 class="card-title m-b-5"><span class="lstick"></span>Dados Funcionais</h3>
	                </div>
					<!-- informações Profissionais -->
					<div class="panel panel-default">

						<div class="panel-body">

							<div class="row">
								<div class="form-group col-md-3">
									<label>Matrícula *</label>
									<input id="matricula" type="text" name="filiado[matricula]" class="form-control" placeholder="000000-0" value="{{ old('filiado.matricula') }}">
								</div>

								<div class="form-group col-md-4">
									<label>Cargo</label>
									<input type="text" name="filiado[cargo]" class="form-control" placeholder="" value="{{ old('filiado.cargo') }}">
								</div>

								<div class="form-group col-md-5">
									<label>Local de Trabalho</label>
									<input type="text" name="filiado[local_trabalho]" class="form-control" placeholder="" value="{{ old('filiado.local_trabalho') }}">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Classe</label>
									<input type="text" name="filiado[classe]" class="form-control" placeholder="" value="{{ old('filiado.classe') }}">
								</div>

								<div class="form-group col-md-4">
									<label>Nível</label>
									<input type="text" name="filiado[nivel]" class="form-control" placeholder="" value="{{ old('filiado.nivel') }}">
								</div>

								<div class="form-group col-md-4">
									<label>Data de admissão</label>
									<input type="text" name="filiado[data_admissao]" id="data_admissao" class="form-control data" placeholder="" value="{{ old('filiado.data_admissao') }}">
								</div>
							</div>
						</div>
					</div>
					<!-- Informações profissionais -->
				</div>
			</div>
			<input type="submit" class="btn btn-success" value="Cadastrar">
		</form>			
	</div>
@endsection

@section('scripts')
	<script type="text/javascript" src=" {{ asset('js/jquery.mask.min.js') }} "></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#matricula').mask('000000-0');
			$('.cpf').mask('000.000.000-00');
			$('.data').mask('00/00/0000');
			$('.fone').mask('(00) 00000-0000');
			$('.cep').mask('00.000-000');

			$('#cadastro_filiado').on('submit', function(evt){
				evt.preventDefault();

				let data_nascimento = '';
				let data_admissao = '';

				if (this.data_nascimento.value !== '') {
					data_nascimento = this.data_nascimento.value.substring(6, 10)+"-"+this.data_nascimento.value.substring(3, 5)  + "-" +  this.data_nascimento.value.substring(0, 2);
					this.data_nascimento.value  = data_nascimento;
					console.info(data_nascimento);
				}

				if (this.data_admissao.value !== '') {
					data_admissao = this.data_admissao.value.substring(6, 10)+"-"+this.data_admissao.value.substring(3, 5)  + "-" +  this.data_admissao.value.substring(0, 2);
					this.data_admissao.value = data_admissao;
					console.info(data_admissao);
				}

				this.submit();
			});
		});
	</script>
@endsection