@extends('layouts.dashboard')

@section('title', 'Editar Filiado')

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div>
                    <h3 class="card-title m-b-5"><span class="lstick"></span>Alterar Filiado</h3>
                    <h6 class="card-subtitle">Dados Pessoais</h6>
                </div>
				<form action="{{ route('filiado.update', ['id' => $filiado->id ]) }}" id="editar_filiado" name="cadastro_filiado" method="POST">
					{{ method_field('PUT') }}
					{{ csrf_field() }}
			    	
			    	<div class="panel panel-default">
					  	<div class="panel-body">

							<div class="row">
								<div class="form-group col-md-4">
									<label>CPF</label>
									<input type="text" name="pessoa[cpfCnpj]" class="form-control cpf" placeholder="000.000.000-00" disabled="disabled" value="{{ $filiado->pessoa()->first()->cpfCnpj }}">
								</div>

								<div class="form-group col-md-8">
									<label>Nome completo</label>
									<input type="text" name="pessoa[nome]" class="form-control" placeholder="" value="{{ $filiado->pessoa()->first()->nome }}">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>RG</label>
									<input type="text" name="pessoa[rg]" class="form-control rg" placeholder="" value="{{ $filiado->pessoa()->first()->rg }}">
								</div>

								<div class="form-group col-md-4">
									<label>Órgão expeditor</label>
									<input type="text" name="pessoa[orgao_expeditor]" class="form-control" placeholder="Exemplo: SSP-PI" value="{{ $filiado->pessoa()->first()->orgao_expeditor }}">
								</div>

								<div class="form-group col-md-4">
									<label>Data de nascimento</label>
									<input type="text" name="pessoa[data_nascimento]" id="data_nascimento" class="form-control data" placeholder="dd/mm/aaaa" value="{{ $filiado->pessoa()->first()->data_nascimento }}">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>CEP</label>
									<input type="text" name="pessoa[cep]" class="form-control cep" placeholder="Exemplo: 64000-000" value="{{ $filiado->pessoa()->first()->cep }}">
								</div>

								<div class="form-group col-md-8">
									<label>Endereço</label>
									<input type="text" name="pessoa[endereco]" class="form-control" placeholder="Exemplo: Av. Santos Drumond, 200" value="{{ $filiado->pessoa()->first()->endereco }}">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Bairro</label>
									<input type="text" name="pessoa[bairro]" class="form-control" placeholder="" value="{{ $filiado->pessoa()->first()->bairro }}">
								</div>

								<div class="form-group col-md-6">
									<label>Cidade</label>
									<input type="text" name="pessoa[cidade]" class="form-control" placeholder="" value="{{ $filiado->pessoa()->first()->cidade }}">
								</div>

								<div class="form-group col-md-2">
									<label>UF</label>
									<input type="text" name="pessoa[uf]" class="form-control" placeholder="" value="{{ $filiado->pessoa()->first()->uf }}">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Telefone</label>
									<input type="text" name="pessoa[telefone]" class="form-control fone" placeholder="(86) 0000-0000" value="{{ $filiado->pessoa()->first()->telefone }}">
								</div>

								<div class="form-group col-md-4">
									<label>e-mail</label>
									<input type="text" name="pessoa[email]" class="form-control email" placeholder="" value="{{ $filiado->pessoa()->first()->email }}">
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-body">
				<h3 class="card-title m-b-5"><span class="lstick"></span>Dados Funcionais</h3>
			<!-- informações Profissionais -->
			<div class="panel panel-default">
			  	<div class="panel-body">
			    
					<div class="row">
						<div class="form-group col-md-3">
							<label>Matrícula *</label>
							<input type="text" name="filiado[matricula]" class="form-control" placeholder="000000-0" disabled="disabled" value="{{ $filiado->matricula }}">
						</div>

						<div class="form-group col-md-4">
							<label>Local de Trabalho</label>
							<input type="text" name="filiado[local_trabalho]" class="form-control" placeholder="" value="{{ $filiado->local_trabalho }}">
						</div>

						<div class="form-group col-md-5">
							<label>Cargo</label>
							<input type="text" name="filiado[cargo]" class="form-control" placeholder="" value="{{ $filiado->cargo }}">
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-4">
							<label>Classe</label>
							<input type="text" name="filiado[classe]" class="form-control" placeholder="" value="{{ $filiado->classe }}">
						</div>

						<div class="form-group col-md-4">
							<label>Nível</label>
							<input type="text" name="filiado[nivel]" class="form-control" placeholder="" value="{{ $filiado->nivel }}">
						</div>

						<div class="form-group col-md-4">
							<label>Data de admissão</label>
							<input type="text" name="filiado[data_admissao]" id="data_admissao" class="form-control data" placeholder="dd/mm/aaaa" value="{{ $filiado->data_admissao }}">
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="demo-checkbox">
								<input type="checkbox" id="basic_checkbox_2" value="1" class="filled-in" @if( $filiado->filiado ) checked="checked" @endif>
								<label for="basic_checkbox_2">Filiado</label>
							</div>
							<input type="hidden" id="ativo" name="filiado[filiado]" value="{{ $filiado->filiado }}">
						</div>	
					</div>
			  	</div>
			</div>
			<!-- Informações profissionais -->	
				</div>
			</div>

			<input type="submit" class="btn btn-success" value="Atualizar">
		</form>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript" src=" {{ asset('js/jquery.mask.min.js') }} "></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.data').mask('00/00/0000');
			$('.fone').mask('(00) 00000-0000');
			$('.cep').mask('00.000-000');

			$('#basic_checkbox_2').change(function(){
				if ( $(this).prop('checked') ) {
					$('#ativo').val(1);	
				}else{
					$('#ativo').val(0);
				}
			});

			let data_nascimento = $('#data_nascimento').val();
			if (data_nascimento) {
				var oldy = data_nascimento.substring(6, 10);
				var oldm = data_nascimento.substring(3, 5);
				var oldd = data_nascimento.substring(0, 2);
				
				var newy = oldd + oldm;
				var newm = oldy.substring(0,2);
				var newd = oldy.substring(2,4);
				
				$('#data_nascimento').val(newd + '/' + newm + "/" + newy);
			}

			let data_admissao = $('#data_admissao').val();
			if (data_admissao) {
				oldy = data_admissao.substring(6, 10);
				oldm = data_admissao.substring(3, 5);
				oldd = data_admissao.substring(0, 2);
				
				newy = oldd + oldm;
				newm = oldy.substring(0,2);
				newd = oldy.substring(2,4);
				
				$('#data_admissao').val(newd + '/' + newm + "/" + newy);
			}

			$('#editar_filiado').on('submit', function(evt){
				evt.preventDefault();

				let data_nascimento = '';
				let data_admissao = '';

				if (this.data_nascimento.value !== '') {
					data_nascimento = this.data_nascimento.value.substring(6, 10)+"-"+this.data_nascimento.value.substring(3, 5)  + "-" +  this.data_nascimento.value.substring(0, 2);
					this.data_nascimento.value  = data_nascimento;
					console.info(data_nascimento);
				}

				if (this.data_admissao.value !== '') {
					data_admissao = this.data_admissao.value.substring(6, 10)+"-"+this.data_admissao.value.substring(3, 5)  + "-" +  this.data_admissao.value.substring(0, 2);
					this.data_admissao.value = data_admissao;
					console.info(data_admissao);
				}

				this.submit();
			});
		});
	</script>
@endsection