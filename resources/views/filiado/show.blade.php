@extends('layouts.dashboard')

@section('title', 'Novo Filiado')

@section('header-left-controls')
	<!-- Controles da esquerda -->
@endsection

@section('header-right-controls')
	
@endsection


@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<h3 class="card-title m-b-5 pull-left"><span class="lstick"></span>Dados do Filiado</h3>
                
		                <div class="pull-right">
		                	<a href="{{ route('filiado.edit', ['id' => $filiado->id]) }}" class="btn btn-primary"> <span class="mdi mdi-account-edit"></span> Editar</a>
							<a href="#" class="btn btn-danger"><span class="mdi mdi-delete"></span> Excluir</a>
		                </div>	
					</div>
				</div>
				<h6>DADOS PESSOAIS</h6>
				<div class="panel panel-default">
					<div class="panel-body">
						
						<p><strong class="font-weight-bold">CPF:</strong> {{ $filiado->pessoa()->first()->cpfCnpj }}</p>
						<p><strong class="font-weight-bold">Nome:</strong> {{ $filiado->pessoa()->first()->nome }}</p>
						<p><strong class="font-weight-bold">RG:</strong> {{ $filiado->pessoa()->first()->rg }}</p>
						<p><strong class="font-weight-bold">Orgão Expeditor:</strong> {{ $filiado->pessoa()->first()->orgao_expeditor }}</p>
						<p><strong class="font-weight-bold">Data de Nascimento:</strong> {{ $filiado->pessoa()->first()->data_nascimento }}</p>
						<p><strong class="font-weight-bold">Endereço:</strong> {{ $filiado->pessoa()->first()->endereco }}</p>
						<p><strong class="font-weight-bold">Bairro:</strong> {{ $filiado->pessoa()->first()->bairro }}</p>
						<p><strong class="font-weight-bold">Cidade:</strong> {{ $filiado->pessoa()->first()->cidade }}</p>
						<p><strong class="font-weight-bold">UF:</strong> {{ $filiado->pessoa()->first()->uf }}</p>
						<p><strong class="font-weight-bold">CEP:</strong> {{ $filiado->pessoa()->first()->cep }}</p>
						<p><strong class="font-weight-bold">Telefone:</strong> {{ $filiado->pessoa()->first()->telefone }}</p>
						<p><strong class="font-weight-bold">Email:</strong> {{ $filiado->pessoa()->first()->email }}</p>
					</div>
				</div>		
			</div>
		</div>
		
		<div class="card">
			<div class="card-body">
				<h3 class="card-title m-b-5"><span class="lstick"></span>Dados Funcionais</h3>
				
				<div class="panel panel-default">
					<div class="panel-body">
						<p><strong class="font-weight-bold">Matrícula:</strong> {{ $filiado->matricula }}</p>
						<p><strong class="font-weight-bold">Cargo:</strong> {{ $filiado->cargo }}</p>
						<p><strong class="font-weight-bold">Local de Trabalho:</strong> {{ $filiado->local_trabalho }}</p>
						<p><strong class="font-weight-bold">Classe:</strong> {{ $filiado->classe }}</p>
						<p><strong class="font-weight-bold">Nível:</strong> {{ $filiado->nivel }}</p>
						<p><strong class="font-weight-bold">Filiado:</strong> @if($filiado->filiado) Sim <strong class="font-weight-bold">desde</strong> {{ $filiado->data_admissao }} @else Não @endif</p>
					</div>
				</div>	
			</div>
		</div>
	</div>

	
@endsection