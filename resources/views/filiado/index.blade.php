@extends('layouts.dashboard')

@section('title', 'Gerenciamento de Filiados')

@section('header-left-controls')
	<!-- Controles da esquerda -->
@endsection



@section('content')

<div class="card">
	<div class="card-body">
		<h4 class="card-title">Filiação</h4>
        <h6 class="card-subtitle">Gerenciamento de Filiados</h6>
        <a href="{{ route('filiado.create') }}" class="btn btn-info pull-right"> <span class="fa fa-plus"></span> <span class="fa fa-user-circle"></span> P. Física</a> 
        <a href="{{ route('pessoajuridica.create') }}" class="btn btn-primary pull-right"> <span class="fa fa-plus"></span> <span class="fa fa-user-circle-o"></span> P. Jurídica</a> 
		<a href="{{ route('tecnico.create') }}" class="btn btn-success pull-right"> <span class="fa fa-plus"></span> <span class="fa fa-user-o"></span> Técnico</a>
        
        <div class="table-responsive m-t-40">
			<table id="datatable" class="display nowrap table table-hover table-striped table-bordered table-responsive" cellspacing="0">
				<thead>
					<tr>
						<!-- <th> <input type="checkbox" name="select-all-customer"> </th> -->
						<th>Matrícula</th>
						<th>Nome</th>
						<th>Local de Trabalho</th>
						<th>Cargo</th>
						<th>Email</th>
						<th>Filiado</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tbody>

					@foreach ($filiados as $filiado)
						<tr>
							<!-- <td> <input type="checkbox" name="select-customer"> </td> -->
							<td>
								{{ $filiado->matricula }}
							</td>
							<td>{{ $filiado->nome }}</td>
							<td>{{ $filiado->pessoa()->first()->nome }}</td>
							<td>{{ $filiado->cargo }}</td>
							<td>
								{{ $filiado->pessoa()->first()->email }}
							</td>
							<td>
								{{ $filiado->filiado }}
							</td>
							<td>
								<div class="btn-group">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="mdi mdi-account-settings"></span>
                                    </button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <a class="dropdown-item" href="{{ route('filiado.show', ['id' => $filiado->id]) }}">
                                        	<span class="mdi mdi-account-card-details"></span> Detalhar
                                        </a>

                                        <a class="dropdown-item" href="{{ route('filiado.edit', ['id' => $filiado->id]) }}">
                                        	<span class="mdi mdi-account-edit"></span> Alterar
                                        </a>
                                        <a class="dropdown-item" href="{{ route('filiado.activate', ['id' => $filiado->id]) }}">
                                        	<span class="mdi mdi-face-profile"></span> Tornar Técnico
                                        </a>
                                    </div>
                                </div>
							</td>
						</tr>
					@endforeach

				</tbody>

				<tfoot>
					<tr>
						<th>Tipo</th>
						<th>CPF/CNPJ</th>
						<th>Nome</th>
						<th>Email</th>
						<th>Filiação</th>
						<th>Ações</th>
					</tr>
				</tfoot>
			</table>  	
        </div>
	</div>
</div>

@endsection

@section('scripts')
	<script src="{{ asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
	<!-- <script src="{{ asset('js/datatables.min.js') }}"></script> -->
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
	<script type="text/javascript">
		var exportTitle = 'Filiados';
		
		$(document).ready(function() {
			
			$('#datatable tfoot th').each( function (index, value) {
		        var title = $(this).text();

		        if (index == 0) {
		        	
		        }else if (title != 'Ações'){
		        	$(this).html( '<input class="form-control" type="text" placeholder="Buscar por '+title+'" />' );
		        }

		    } );
			
			var counter = 0;

			var table = $('#datatable').DataTable({
				initComplete: function () {
		            this.api().columns().every( function () {
		                var that = this;
		                
		                if (counter == 0) {
		                	var select = $('<select class="form-control"><option value="">Por Tipo</option></select>')
			                    .appendTo( $(that.footer()).empty() )
			                    .on( 'change', function () {
			                        var val = $.fn.dataTable.util.escapeRegex(
			                            $(this).val()
			                        );
			 
			                        that
			                            .search( val ? '^'+val+'$' : '', true, false )
			                            .draw();
			                    } );
			 
			                that.data().unique().sort().each( function ( d, j ) {
			                    select.append( '<option value="'+d+'">'+d+'</option>' )
			                } );
		                }else{
		                	$( 'input', this.footer() ).on( 'keyup change', function () {
					            if ( that.search() !== this.value ) {
					                that
					                    .search( this.value )
					                    .draw();
					            }
					        } );
		                }

				        counter += 1;
		            } );
		        },
		    	dom: 'Bfrtip',
		        buttons: [
					{
						extend: 'csv',
						title: exportTitle
					}, 
					{
						extend: 'excel',
						title: exportTitle
					}, 
					{
						extend: 'pdf',
						title: exportTitle
					}, 
					{
						extend: 'print',
						title: exportTitle
					}
		        ],
		    	/*'columnDefs': [
	    			{"searchable": true, "targets": 2}
		    	],*/
		    	'language':{
		    		"search":"Buscar registros",
		    		"emptyTable": "Não registros",
		    		"zeroRecords": "Não foram encontrados registros",
		    		"lengthMenu": "Exbir _MENU_ registros",
		    		"infoFiltered": "(filtrados de _MAX_ registros)",
		    		"info": "Exibindo _START_ a _END_ de _TOTAL_ registros",
		    		"infoEmpty": "Exibindo 0 a 0 de 0 registros",
		    		"paginate": {
		    			"first": "Primeira página",
		    			"last": "Última página",
		    			"next": "Próximo",
		    			"previous": "Anterior"
		    		}
		    	}
			});
		} );
	</script>

@endsection