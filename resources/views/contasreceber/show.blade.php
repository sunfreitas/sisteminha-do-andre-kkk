@extends('layouts.dashboard')

@section('title', 'Dados da conta ')

@section('header-left-controls')
	<!-- Controles da esquerda -->
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<h3 class="card-title m-b-5 pull-left"><span class="lstick"></span>Dados da Conta</h3>

						<div class="pull-right">
							<a href="{{ route('contasreceber.edit', ['id' => $conta->codigo]) }}" class="btn btn-primary"><span class="mdi mdi-account-edit"></span> Editar</a>
							<a href="#" class="btn btn-danger"><span class="mdi mdi-delete"></span>Excluir</a>	
						</div>
					</div>
				</div>

				<div class="panel panel-default">
				<div class="panel-heading">Dados do tipo de conta</div>
				<div class="panel-body">
					<p><strong>Código:</strong> {{ $conta->codigo }}</p>
					<p><strong>Parcela:</strong> {{ $conta->parcela }}</p>
					<p><strong>Valor:</strong> R${{ $conta->valor }}</p>
					<p><strong>Descrição:</strong> {{ $conta->descricao }}</p>
					<p><strong>Data do vencimento:</strong> {{ $conta->data_vencimento }}</p>
					<p><strong>Data do pagamento:</strong> {{ $conta->data_pagamento }}</p>
					<p><strong>Desconto:</strong> {{ $conta->desconto }}</p>
					<p><strong>Valor pago:</strong> R${{ $conta->valor_pago }}</p>
					<p><strong>Forma de pagamento:</strong> {{ $conta->forma_pagamento }}</p>
					<p><strong>Observação:</strong> {{ $conta->observacao }}</p>
				</div>
			</div>
			</div>
		</div>
	</div>

	
@endsection