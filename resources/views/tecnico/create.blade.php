@extends('layouts.dashboard')

@section('title', 'Novo Técnico')


@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div>
                    <h3 class="card-title m-b-5"><span class="lstick"></span>Cadastrar Técnico</h3>
                    <h6 class="card-subtitle">Dados Pessoais</h6>
                </div>
				<form action="{{ route('filiado.store') }}" method="POST" name="cadastro_filiado" id="cadastro_tecnico">

					{{ csrf_field() }}

					<div class="panel panel-default">
						<div class="panel-heading"></div>
						<div class="panel-body">
                            <input name="pessoa[tipo_pessoa]" type="hidden" value="0">

							<div class="row">
								<div class="form-group col-md-4">
									<label>CPF *</label>
									<input type="text" name="pessoa[cpfCnpj]" class="form-control cpf" required="required">
								</div>

								<div class="form-group col-md-8">
									<label>Nome completo *</label>
									<input type="text" name="pessoa[nome]" class="form-control" placeholder="" required="required">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>RG</label>
									<input type="text" name="pessoa[rg]" class="form-control rg" placeholder="">
								</div>

								<div class="form-group col-md-4">
									<label>Órgão expeditor</label>
									<input type="text" name="pessoa[orgao_expeditor]" class="form-control" placeholder="Exemplo: SSP-PI">
								</div>

								<div class="form-group col-md-4">
									<label>Data de nascimento</label>
									<input type="text" name="pessoa[data_nascimento]"  id="data_nascimento" class="form-control data" placeholder="dd/mm/aaaa">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>CEP</label>
									<input type="text" name="pessoa[cep]" class="form-control cep" placeholder="Exemplo: 64000-000">
								</div>

								<div class="form-group col-md-8">
									<label>Endereço</label>
									<input type="text" name="pessoa[endereco]" class="form-control" placeholder="Exemplo: Av. Santos Drumond, 200">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Bairro</label>
									<input type="text" name="pessoa[bairro]" class="form-control" placeholder="">
								</div>
									
								<div class="form-group col-md-6">
									<label>Cidade</label>
									<input type="text" name="pessoa[cidade]" class="form-control" placeholder="">
								</div>

								<div class="form-group col-md-2">
									<label>UF</label>
									<input type="text" name="pessoa[uf]" class="form-control" placeholder="">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Telefone</label>
									<input type="text" name="pessoa[telefone]" class="form-control fone" placeholder="(86) 0000-0000">
								</div>

								<div class="form-group col-md-4">
									<label>E-mail</label>
									<input type="text" name="pessoa[email]" class="form-control email" placeholder="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-body">
					<div>
	                    <h3 class="card-title m-b-5"><span class="lstick"></span>Dados Funcionais</h3>
	                </div>
					<!-- informações Profissionais -->
					<div class="panel panel-default">

						<div class="panel-body">

							<div class="row">
								<div class="form-group col-md-3">
									<label>Matrícula *</label>
									<input id="matricula" type="text" name="filiado[matricula]" class="form-control" placeholder="000000-0" required="required">
								</div>

								<div class="form-group col-md-4">
									<label>Cargo</label>
									<input type="text" name="filiado[cargo]" class="form-control" placeholder="">
								</div>

								<div class="form-group col-md-5">
									<label>Lotação</label>
									<input type="text" name="filiado[local_trabalho]" class="form-control" placeholder="">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Classe</label>
									<input type="text" name="filiado[classe]" class="form-control" placeholder="">
								</div>

								<div class="form-group col-md-4">
									<label>Nível</label>
									<input type="text" name="filiado[nivel]" class="form-control" placeholder="">
								</div>

								<div class="form-group col-md-4">
									<label>Data de admissão</label>
									<input type="text" name="filiado[data_admissao]" id="data_admissao" class="form-control data" placeholder="">
								</div>
							</div>
						</div>
					</div>
					<!-- Informações profissionais -->
				</div>
			</div>

			<div class="card">
				<div class="card-body">
					<div>
	                    <h3 class="card-title m-b-5"><span class="lstick"></span>Filiação</h3>
	                </div>
	                <div class="row">
		                <div class="form-group col-md-4">
							<label>Situação *</label>
							<select name="filiado[situacao]" class="form-control">
								<option value="1" selected="selected">Inativo</option>
								<option value="2">Ativo</option>
								<option value="3">Pensionista</option>
							</select>
						</div>

						<div class="form-group col-md-4">
							<label>Data Filiação</label>
							<input type="text" name="filiado[data_filiacao]" class="form-control" placeholder="YYYY-mm-dd">
						</div>

						<div class="form-group col-md-4">
							<label>Data Desfiliação</label>
							<input type="text" name="filiado[data_desfiliacao]" class="form-control" placeholder="YYYY-mm-dd">
						</div>
					</div>
	            </div>
	        </div>
			<input type="submit" class="btn btn-success" value="Cadastrar">
		</form>			
	</div>
@endsection

@section('scripts')
	<script type="text/javascript" src=" {{ asset('js/jquery.mask.min.js') }} "></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#matricula').mask('000000-0');
			$('.cpf').mask('000.000.000-00');
			$('.data').mask('00/00/0000');
			$('.fone').mask('(00) 00000-0000');
			$('.cep').mask('00.000-000');

			$('#cadastro_tecnico').on('submit', function(evt){
				evt.preventDefault();

				let data_nascimento = '';
				let data_admissao = '';
				let data_filiacao = '';
				let data_desfiliacao = '';

				if (this.data_nascimento.value !== '') {
					data_nascimento = this.data_nascimento.value.substring(6, 10)+"-"+this.data_nascimento.value.substring(3, 5)  + "-" +  this.data_nascimento.value.substring(0, 2);
					this.data_nascimento.value  = data_nascimento;
					console.info(data_nascimento);
				}

				if (this.data_admissao.value !== '') {
					data_admissao = this.data_admissao.value.substring(6, 10)+"-"+this.data_admissao.value.substring(3, 5)  + "-" +  this.data_admissao.value.substring(0, 2);
					this.data_admissao.value = data_admissao;
					console.info(data_admissao);
				}

				/*if (this.data_filiacao.value !== '') {
					data_filiacao = this.data_filiacao.value.substring(6, 10)+"-"+this.data_filiacao.value.substring(3, 5)  + "-" +  this.data_filiacao.value.substring(0, 2);
					this.data_filiacao.value = data_filiacao;
					console.info(data_filiacao);
				}

				if (this.data_desfiliacao.value !== '') {
					data_desfiliacao = this.data_desfiliacao.value.substring(6, 10)+"-"+this.data_desfiliacao.value.substring(3, 5)  + "-" +  this.data_desfiliacao.value.substring(0, 2);
					this.data_desfiliacao.value = data_desfiliacao;
					console.info(data_desfiliacao);
				}*/

				this.submit();
			});
		});
	</script>
@endsection